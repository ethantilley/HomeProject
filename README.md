# Everlight

Play as a spirit orb on your way home with your companion journeying into a dark cave. Your are suddenly separated and will need to find another way through. You will maneuver through the cave system until reunited with your companion. Once together again, you will make your way out of the cave and into a magical forest where you will find a secret community just where you belong.

## Built With

* [Unity](http://www.unity.com/) - Game Engine 
* [Cinimachine](https://assetstore.unity.com/packages/essentials/cinemachine-79898) - Camera Work
* [HackN'Plan](https://hacknplan.com/) - Task Management Softwear
* [SourceTree](https://www.sourcetreeapp.com/) - Repository softwear paired with gitlab
* [VisualStudio](https://visualstudio.microsoft.com/) - For all coding purposes

## Authors

* **Ethan Tilley** - *Lead programmer* - [@EthanTilley_](https://twitter.com/EthanTilley_)
* **Sey Atkinson** - *Lead Designer* - [@Sey_Atkinson](https://twitter.com/Sey_Atkinson)
* **Brea Pringle** - *Audio Engineer* - [@june_slug](https://twitter.com/june_slug)

## Acknowledgments

* Hat tip to our lectures (Iain McManus & Aaron Williams) who fasilitated us through this project. <3
* Inspired by indie titles Abzu, Forerunner, Sanctuary and Beautiful.
