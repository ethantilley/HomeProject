﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelRoll : MonoBehaviour
{
    public float barrelRollSpeed = 0.8f;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space) || Input.GetKey("joystick button 0"))
        {
            StartBarrelRoll();
        }
    }
    void StartBarrelRoll()
    {
        float startRotation = transform.eulerAngles.z;
        float endRotation = startRotation + 360.0f;
        
        float zRotation = Mathf.Lerp(startRotation, endRotation, barrelRollSpeed * Time.deltaTime) % 360.0f;
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, zRotation);

    }
}
