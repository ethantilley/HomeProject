﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UiManager : MonoBehaviour {

    public GameObject pausePanel;
    public GameObject movementUIPrompt;

    bool isPaused = false;
    bool keyPressed = false;

    float secondsCount;
    float UiPromptTimer = 5;

    // Update is called once per frame
    void Update () {

        Pause();

        secondsCount += Time.deltaTime;

        //Checks if any input
        if (Input.anyKey == true)
        {
            movementUIPrompt.SetActive(false);
        }
    }

    public void Pause()
    {
        if (!isPaused)
        {
            if (Input.GetKeyDown("escape") || Input.GetKeyDown("joystick button 7"))
            {
                Debug.Log("Paused");
                Time.timeScale = 0;
                isPaused = true;
                pausePanel.SetActive(true);
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }
        else
        {
            if (Input.GetKeyDown("escape") || Input.GetKeyDown("joystick button 7"))
            {
                Debug.Log("Unpaused");
                Time.timeScale = 1;
                isPaused = false;
                pausePanel.SetActive(false);
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
        }
    }

  
    public void ActivateSensSlider(GameObject slider)
    {
        slider.SetActive(!slider.activeInHierarchy);
    }
    public void OnClick_Quit()
    {
        Application.Quit();

    }

    public void OnClick_Reset(int levelToLoad)
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(levelToLoad);
    }

    public void InvertControlls()
    {
        PlayerMovement.InvertControls();
        CameraLook.InvertCamControls();
    }
}
