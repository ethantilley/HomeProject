﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanternSwitch : MonoBehaviour {

    public GameObject lanternLighting;
    public Rotater gemSpinnyThing;
    bool activated;
    public GameObject[] orbsThisActivates;

    private void Start()
    {
        lanternLighting.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && !activated)
        {
            activated = true;
            ForestManager.instance.LanternActivated(orbsThisActivates);
            lanternLighting.SetActive(true);
            gemSpinnyThing.enabled = true;
        }
    }

}
