﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommunityOrb : MonoBehaviour {

    public bool moveingOnTrack = false;
    public float trackSpeed = 20, followSpeed = 30;

    public bool canFollowPlayer = true;

    public FollowPoint target;

    public float followTime = 10, randomnessOffset = 2f;
    public float followCoolDown = 60;
    float currentCoolDown;
    
    public MovementState currentMoveState = MovementState.TrackMovement;
    public enum MovementState
    {
        TrackMovement,
        FollowMovement
    }

    private void Update()
    {
        if (currentCoolDown>0)
        {
            currentCoolDown -= Time.deltaTime;
        }   
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        switch (currentMoveState)
        {
            case MovementState.TrackMovement:
                if(transform.position != transform.parent.position)
                    MoveToTarget(transform.parent, transform, 7);
                else
                {
                    transform.parent.GetComponent<Cinemachine.CinemachineDollyCart>().m_Position += trackSpeed * Time.deltaTime;
                }
                break;

            case MovementState.FollowMovement:

                transform.position = Vector3.Lerp(transform.position, target.transform.position, followSpeed * Time.deltaTime);
                // MoveToTarget(target.transform, transform, followSpeed);
                break;

            default:
                break;
        }       
	}

    //public void SwitchMovementTypes()
    //{
    //    switch (currentMoveState)
    //    {
    //        case MovementState.TrackMovement:
    //            currentMoveState = MovementState.FollowMovement;
    //            break;
    //        case MovementState.FollowMovement:
    //            target = null;
    //            currentMoveState = MovementState.TrackMovement;
    //            break;
    //        default:
    //            break;
    //    }
    //}

    public bool isFollowing;
    public void SetTarget(FollowPoint newTarget)
    {
        if (isFollowing || currentCoolDown > 0)
            return;

        isFollowing = true;

        target = newTarget;
        currentMoveState = MovementState.FollowMovement;

        StartCoroutine(FollowTime());
    }
    IEnumerator FollowTime()
    {
       
        yield return new WaitForSeconds(followTime + Random.Range(-randomnessOffset, randomnessOffset));
        target.UnAssignOccupant();
        target = null;
        currentCoolDown = followCoolDown;
        isFollowing = false;
       
        currentMoveState = MovementState.TrackMovement;
    }

    public void MoveToTarget(Transform destination, Transform gameObj, float speed)
    {       
        gameObj.position = Vector3.MoveTowards(gameObj.position, destination.position, speed * Time.deltaTime);
    }

}
