﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Alters the trigger enterings hitpoints if it has a HitPointController
/// </summary>
public class DamageDealer : MonoBehaviour
{
    public int dmgOnHit = 1;
    public FMODUnity.StudioEventEmitter fmodEvent;

    private void OnTriggerEnter(Collider other)
    {
        var hpc = other.GetComponent<HitPointController>();

        if(hpc != null && !hpc.isInvuln)
        {
            hpc.TakeDamage(dmgOnHit);
            if (fmodEvent != null)
                fmodEvent.Play();
        }
    }
}
