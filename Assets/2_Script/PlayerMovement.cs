﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;
using System.Diagnostics;
using System;

public class PlayerMovement : MonoBehaviour
{
    public static void InvertControls()
    {
        invertYAxis = !invertYAxis;
    }

    public bool lockMovement = true;

    private Stopwatch caveTimer = new Stopwatch(), freedomTimer = new Stopwatch();
    public float eventCoolDown = 3;
    private float eventCurrentCoolDown;

    public static bool invertYAxis = false;

    [Header("Contrainted Movement"), Space(10)]

    [Tooltip("The players current forward speed  through the cave system")]
    public float currentForwardSpeed = 10;
    public float forwardSpeed = 10;

    [Tooltip("The players maximum forward speed it can reach through the cave system")]
    public float maximumForwardSpeed = 10;

    [Tooltip("The players base/minumum forward speed through the cave system")]
    public float baseForwardSpeed = 3;

    [Tooltip("The speed the the speed is inceased by of time")]
    public float restoreSpeed = 1;

    [Tooltip("The players usual dodge speed through the cave system")]
    public float sideMoveSpeed = 2f;

    [Range(0f, 1f), Tooltip("The players speed smoothing through the cave system")]
    public float sideMovementDampinging = 1;

    [Tooltip("How long the player is stuned for in the cave system")]
    public float stunCoolDown = .5f;

    public float stunKnockback = 3, stunKnockDampining = 3;
    protected float desiredPos;
    bool isBeingKnocked;
    protected float currStunTime;

    [Header("Freeform Movement"), Space(10)]

    [Tooltip("The players freeform movement speed")]
    public float freeformSpeed = 20;

    public float currentFreeformSpeed = 20;

    [Tooltip("How much the player's freeform movement speed is multiplied by when sprinting")]
    public float sprintMultiplier = 1.5f;

    [Range(0f, 1f), Tooltip("The players freeform movement speed smoothing amount, higher - the sharper")]
    public float movementStepAmount = .5f;

    public AnimationCurve accelerationCurve;

    [Tooltip("The maximum distance the players target pos will be from the player, higher - the sharper")]
    public float maxTargetDistance = 0.5f;

    public FollowPoint[] points;

    [Header("Analyitics Stuff")]
    public List<string> hazardsCollidedWith = new List<string>();
    public float timeInCave, timeOutOfCave;
    bool sprinting = false;
    Vector3 targetLocation;
    bool startMovement = false;
    bool flippingPlayer;
<<<<<<< HEAD

   public FMODUnity.StudioEventEmitter interaction;
=======
    Rigidbody rb;
    public FMODUnity.StudioEventEmitter interaction;

>>>>>>> e7c686b5f6699887a47c21e7e3d30928a216dd1c

    //SphereCollider PlayerCollider
    //{
    //    get
    //    {
    //        if (GetComponent<SphereCollider>())
    //            return GetComponent<SphereCollider>();
    //        else
    //        {
    //        //  SphereCollider newColl = gameObject.AddComponent<SphereCollider>();
    //            return newColl;
    //        }
    //    }
    //}
    //Vector3 DirLerp { get { return Vector3.Lerp(transform.position, targetLocation, movementStepAmount * Time.deltaTime) - transform.position; } }


    //  public Vector3 desiredVector;

    public float FindDifference(float nr1, float nr2)
    {
        return Mathf.Abs(nr1 - nr2);
    }

    private void Update()
    {
        if (isBeingKnocked)
        {

            //  iTween.m
            if (dolly.m_Position != desiredPos)
                dolly.m_Position = Mathf.Lerp(dolly.m_Position, desiredPos, stunKnockDampining * Time.deltaTime);

            if (FindDifference(dolly.m_Position, desiredPos) < 5)
                isBeingKnocked = false;


        }
        if (lockMovement && !startMovement && Input.anyKeyDown)
        {
            startMovement = true;
        }

        if (Input.GetKeyDown(KeyCode.R))
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        if (lockMovement && dolly.m_Position >= dolly.m_Path.PathLength && eventCurrentCoolDown <= 0)
        {
            eventCurrentCoolDown = eventCoolDown;
            caveTimer.Stop();
            timeInCave = caveTimer.ElapsedMilliseconds / 60;
            EventManager.CallSwitch();
            freedomTimer.Start();
        }

        if (eventCurrentCoolDown > 0)
        {
            eventCurrentCoolDown -= Time.deltaTime;
        }

        if (currentForwardSpeed >= baseForwardSpeed && currentForwardSpeed <= maximumForwardSpeed)
        {
            currentForwardSpeed += Time.deltaTime * restoreSpeed;
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {

            currentFreeformSpeed = freeformSpeed * sprintMultiplier;
            UnityEngine.Debug.Log("Sprinting with LShift");

        }
        else
        {
            if ((Input.GetAxis("Sprint_Fire1") != 0))
            {
                currentFreeformSpeed = freeformSpeed * sprintMultiplier;
                UnityEngine.Debug.Log("Sprinting with Right Trigger");
            }
            else
            {
                currentFreeformSpeed = freeformSpeed;
                sprinting = false;
            }
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            currentFreeformSpeed = freeformSpeed;
            UnityEngine.Debug.Log("Now un-Sprinting with LShift");


        }


    }
    IEnumerator TurnPlayerAround(float turnDuration)
    {
        float startRotation = transform.eulerAngles.x;
        float endRotation = startRotation + 180.0f;
        float t = 0.0f;

        while (t < turnDuration)
        {
            t += Time.deltaTime;
            float xRotation = Mathf.Lerp(startRotation, endRotation, t / turnDuration) % 180.0f;
            transform.eulerAngles = new Vector3(xRotation, transform.eulerAngles.y, transform.eulerAngles.z);
            yield return null;
        }
    }

    public IEnumerator StunPlayer()
    {

        currentForwardSpeed = 1;
        yield return new WaitForSeconds(stunCoolDown);
        currentForwardSpeed = baseForwardSpeed;

    }
    protected Cinemachine.CinemachineDollyCart dolly;
    
    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        invertYAxis = false;

        caveTimer.Start();
        currentFreeformSpeed = freeformSpeed;
        if (transform.parent)
            dolly = transform.parent.GetComponent<Cinemachine.CinemachineDollyCart>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.isKinematic = true;
        rb.isKinematic = false;

        if (lockMovement)
        {
            ContrainedMovement();
        }
        else
        {
            FreeFormMovement();
        }
    }

    void ContrainedMovement()
    {

        if (!startMovement)
        {
            return;
        }

        Vector3 desiredVector = transform.localPosition;
        if (!isBeingKnocked)
            dolly.m_Position += currentForwardSpeed * Time.deltaTime;

        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {

            desiredVector.x += Input.GetAxis("Horizontal") * sideMoveSpeed;

            if (!invertYAxis)
                desiredVector.y += Input.GetAxis("Vertical") * sideMoveSpeed;
            else
                desiredVector.y -= Input.GetAxis("Vertical") * sideMoveSpeed;

            transform.localPosition = Vector3.Lerp(transform.localPosition, desiredVector, Time.deltaTime * sideMovementDampinging);

        }
    }


    private void FreeFormMovement()
    {
        // checks to see if there is input first
        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
        }

        float startDistance = Vector3.Distance(transform.position, targetLocation);
        //  gets the players input direction

        Vector3 inputDirection = transform.forward * Input.GetAxis("Vertical");

        UnityEngine.Debug.DrawLine(transform.position, transform.position + transform.forward, Color.green);
        UnityEngine.Debug.DrawLine(transform.position, transform.position + transform.right, Color.red);

        inputDirection += transform.right * Input.GetAxis("Horizontal");

        // setting the new desired direction 
        Vector3 newDesiredLocation = (targetLocation - transform.position) + (inputDirection * (currentFreeformSpeed * Time.deltaTime));
        //
        // Clamps the desired location to a certain amount
        newDesiredLocation = Vector3.ClampMagnitude(newDesiredLocation, maxTargetDistance);

        // setting the target location to the new desired point.
        targetLocation = transform.position + newDesiredLocation;
        UnityEngine.Debug.DrawLine(transform.position, targetLocation, Color.blue);

        float currDistance = Vector3.Distance(transform.position, targetLocation);
        Vector3 _difference = transform.position - targetLocation;
        _difference = _difference.normalized * currDistance;

        float newStep = accelerationCurve.Evaluate(currDistance);

        // slowly moveing the player to the target point
        movementStepAmount = newStep;
        transform.position = Vector3.Lerp(transform.position, targetLocation, movementStepAmount);
        //Rigidbody rb = GetComponent<Rigidbody>();
        //rb.MovePosition(Vector3.Lerp(transform.position, targetLocation, movementStepAmount));

        #region Old Movement
        //if (Input.GetKey(KeyCode.W))
        //{
        //    transform.Translate(Vector3.forward * Time.deltaTime * currentSpeed);
        //}
        //else if (Input.GetKey(KeyCode.S))
        //{
        //    transform.Translate(-Vector3.forward * Time.deltaTime * currentSpeed);
        //}

        //if (Input.GetKey(KeyCode.D))
        //{
        //    transform.Translate(Vector3.right * Time.deltaTime * currentSpeed);
        //}
        //else if (Input.GetKey(KeyCode.A))
        //{
        //    transform.Translate(-Vector3.right * Time.deltaTime * currentSpeed);
        //}
        #endregion

    }

    /// <summary>
    /// Draws Gixmos for debug viewing.
    /// </summary>
    //private void OnDrawGizmos()
    //{
    //    // Desired Position Gizmo
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawWireSphere(targetLocation, PlayerCollider.radius);

    //    // Current Position Gizmo
    //    Gizmos.color = Color.green;
    //    Gizmos.DrawWireSphere(transform.position, PlayerCollider.radius);

    //}

    private void OnEnable()
    {
        EventManager.SwitchMovement += Player_HandleMovementSwitch;
    }

    private void OnDisable()
    {
        EventManager.SwitchMovement -= Player_HandleMovementSwitch;
    }

    public void Player_HandleMovementSwitch()
    {
        // Hard code all the changes that will happen to the player when the player enters cave or leaves...

        //test:
        if (lockMovement)
        {
            targetLocation = transform.position;

            lockMovement = false;
            UnityEngine.Debug.Log("Setting player movement to freefrom movement");
        }
        else
        {
            lockMovement = true;
            UnityEngine.Debug.Log("Setting player movement to contrained movement");
        }

    }

    private void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.CompareTag("MovementSwitcher") && eventCurrentCoolDown <= 0)
        {
            //   desiredVector = new Vector3(-7.8f, 3.7f, transform.position.z);
            //coll.gameObject.transform.position;

            eventCurrentCoolDown = eventCoolDown;
            EventManager.CallSwitch();
        }
   
        if (coll.gameObject.CompareTag("Hazard") && currStunTime <= 0)
        {
            //  CameraController.instance.StartShake(GetComponent<ScreenShake>().properties);


            StartCoroutine(StunPlayer());
            desiredPos = (dolly.m_Position - stunKnockback);
            isBeingKnocked = true;
            if (!hazardsCollidedWith.Contains(SendOutHazardName(coll.gameObject)))
                hazardsCollidedWith.Add(SendOutHazardName(coll.gameObject));
            GetComponent<AnalyticsTracker>().TriggerEvent();
        }

        if (coll.gameObject.CompareTag("CommunityOrb"))
        {
            FollowPoint newPoint;

            for (int i = 0; i < points.Length; i++)
            {
                if (points[i].occupant == null)
                {
                   
                    newPoint = points[i];
                    coll.gameObject.GetComponent<CommunityOrb>().SetTarget(newPoint);
                    newPoint.AssignOccupant(coll.gameObject);
                    break;
                }
            }
        }
    }

    public string SendOutHazardName(GameObject hazard)
    {
        return hazard.name;
    }

    private void OnApplicationQuit()
    {
        freedomTimer.Stop();
        timeOutOfCave = freedomTimer.ElapsedMilliseconds / 60;
    }
}
