﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprintArea : MonoBehaviour {

    public float sprintMultiplier = 2f;
    PlayerMovement pmScript;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            pmScript = other.gameObject.transform.parent.parent.GetComponent<PlayerMovement>();
            pmScript.currentForwardSpeed = pmScript.forwardSpeed * sprintMultiplier;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            pmScript.currentForwardSpeed = pmScript.forwardSpeed;
        }
    }
}
