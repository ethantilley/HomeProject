﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Dev helper to reset a hitpoint controller back to max hp
/// </summary>
public class ResetHitPoints : MonoBehaviour
{
    public HitPointController hpc;
    public KeyCode keyCode = KeyCode.Home;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(keyCode))
        {
            hpc.ResetHP();
        }
    }
}
