﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectCompanion : MonoBehaviour {

    public CompanionController orbToActive;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
            orbToActive.enabled = true;

    }

}
