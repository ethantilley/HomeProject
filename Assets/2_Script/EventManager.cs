﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour {

    public delegate void SwitchPlayerMovement();
    public static event SwitchPlayerMovement SwitchMovement;

   

    public static void CallSwitch()
    {
        if (SwitchMovement != null)
        {
            
            SwitchMovement();
          //  SwitchMovement = null;
        }
    }
}
