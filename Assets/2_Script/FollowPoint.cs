﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPoint : MonoBehaviour {

    public GameObject occupant;
    
    public void AssignOccupant(GameObject newObj)
    {
        occupant = newObj;
        Invoke("UnAssignOccupant", 20);
    }

    public void UnAssignOccupant()
    {
        occupant = null;
    }
}
