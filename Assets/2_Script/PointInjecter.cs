﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class PointInjecter : MonoBehaviour {

    public WayPointData dataToSet;
    Cinemachine.CinemachineSmoothPath pathScript;
    // Use this for initialization
    void Start () {
        pathScript = GetComponent<Cinemachine.CinemachineSmoothPath>();
    
        pathScript.m_Waypoints = new Cinemachine.CinemachineSmoothPath.Waypoint[dataToSet.wayPointPositions.Count];

        for (int pointIndex = 0; pointIndex < dataToSet.wayPointPositions.Count; pointIndex++)
        {
            pathScript.m_Waypoints[pointIndex].position = (dataToSet.wayPointPositions[pointIndex] - transform.position);
        }
    }
}
