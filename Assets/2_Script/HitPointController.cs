﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

public class HitPointController : MonoBehaviour
{
    public float startingHp = 1, currentHP = 1;
    public float invulnTime = 1;
    public bool isInvuln = false;
    private Coroutine invulnCO;

    public string fmodHPParamName = "HPPer";
    public FMODUnity.StudioEventEmitter deadFmodEvent, dmgFmodEvent, invulnFmodEvent, vulnFmodEvent;


    #region events
    //alt method to use events, make these public to show up in the inspector
    private UnityEvent OnDead = new UnityEvent();

    [System.Serializable]
    public class UnityEventFloat : UnityEvent<float> { }
    private UnityEventFloat OnTakeDamage = new UnityEventFloat();


    private UnityEvent OnInvulnerable = new UnityEvent(), OnVulnerable = new UnityEvent();
    #endregion events
    private void Start()
    {
        ResetHP();
    }

    [ContextMenu("ResetHP")]
    public void ResetHP()
    {
        currentHP = startingHp;
        HandleInvulnTime();
    }

    public void HandleInvulnTime()
    {
        if (invulnCO != null) StopCoroutine(invulnCO);
        invulnCO = StartCoroutine(_invulnDelay());
    }

    private IEnumerator _invulnDelay()
    {
        isInvuln = true;
        OnInvulnerable.Invoke();
        SetHPAndFireFmodEvent(invulnFmodEvent);
        yield return new WaitForSeconds(invulnTime);
        OnVulnerable.Invoke();
        SetHPAndFireFmodEvent(vulnFmodEvent);
        isInvuln = false;
    }

    public bool TakeDamage(float dmg)
    {
        if (isInvuln)
            return false;

        currentHP -= dmg;

        if(currentHP <= 0)
        {
            OnDead.Invoke();
            SetHPAndFireFmodEvent(deadFmodEvent);
        }
        else
        {
            OnTakeDamage.Invoke(currentHP);
            SetHPAndFireFmodEvent(dmgFmodEvent);
            HandleInvulnTime();
        }

        return true;
    }

    public void SetHPAndFireFmodEvent(FMODUnity.StudioEventEmitter fmodevent)
    {
        if (fmodevent == null)
            return;

        var res = fmodevent.Params.FirstOrDefault(x => x.Name == fmodHPParamName);
        
        //not there yet, add it
        if(res == null)
        {
            res = new FMODUnity.ParamRef
            {
                Name = fmodHPParamName
            };

            System.Array.Resize<FMODUnity.ParamRef>(ref fmodevent.Params, fmodevent.Params.Length + 1);
            fmodevent.Params[fmodevent.Params.Length - 1] = res;
        }

        //set current percentage hp
        res.Value = currentHP / startingHp;

        fmodevent.Play();
    }
}
