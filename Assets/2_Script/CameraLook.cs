﻿using UnityEngine;
using UnityEngine.UI;
public class CameraLook : MonoBehaviour
{


    public Transform target;

    protected Vector3 _LocalRotation, _LocalPosition, freeformPosition;
    Quaternion freeformRotation;
    [Range(2f, 5f)]
    public float mouseSensitivity = 4f;
    public float mouseDamping = 10f;

    public bool lockCamera = true;

    public bool camDisabled = false;

    public GameObject vcam;
    public Transform newCamPoint;

    public static bool invertYCam = false;

    public static void InvertCamControls()
    {
        invertYCam = !invertYCam;
    }

    // Use this for initialization
    void Start()
    {
        Cursor.visible = false;

        if (transform.parent != null)
            target = transform.parent;
        camDisabled = true;
        freeformPosition = transform.localPosition;
        freeformRotation = transform.localRotation;
    }
<<<<<<< HEAD
	
	// Late Update is called once per frame after update
	void FixedUpdate ()
=======

    // Late Update is called once per frame after update
    void LateUpdate()
>>>>>>> e7c686b5f6699887a47c21e7e3d30928a216dd1c
    {
        //if (lockCamera)
        //{
        //    // constained code here, return after to ignore other code.

        //    _LocalPosition = new Vector3(0, 7, target.position.z - distance);

        //    Quaternion qt = Quaternion.Euler(0, 0, 0);

        //    transform.rotation = Quaternion.Lerp(transform.rotation, qt, Time.deltaTime * mouseDamping);
<<<<<<< HEAD

        //    transform.position = Vector3.Lerp(transform.position, _LocalPosition, Time.deltaTime * mouseDamping);
        //    return;
        //}
        
=======

        //    transform.position = Vector3.Lerp(transform.position, _LocalPosition, Time.deltaTime * mouseDamping);
        //    return;
        //}

>>>>>>> e7c686b5f6699887a47c21e7e3d30928a216dd1c

        if (Input.GetKeyDown(KeyCode.Escape))
            camDisabled = !camDisabled;

<<<<<<< HEAD
        if(!camDisabled && !lockCamera)
        {
            if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
            {
                _LocalRotation.x += Input.GetAxis("Mouse X") * mouseSensitivity;
                _LocalRotation.y -= Input.GetAxis("Mouse Y") * mouseSensitivity;
=======
        if (!camDisabled && !lockCamera)
        {
            if (Input.GetAxis("Mouse X") != 0 || Input.GetAxis("Mouse Y") != 0)
            {
               
                    _LocalRotation.x += Input.GetAxis("Mouse X") * mouseSensitivity;
                if (!invertYCam)
                    _LocalRotation.y -= Input.GetAxis("Mouse Y") * mouseSensitivity;
                else
                    _LocalRotation.y += Input.GetAxis("Mouse Y") * mouseSensitivity;


>>>>>>> e7c686b5f6699887a47c21e7e3d30928a216dd1c
            }
            else if (Input.GetAxis("Controller_Mouse X") != 0 || Input.GetAxis("Controller_Mouse Y") != 0)
            {
                _LocalRotation.x += Input.GetAxis("Controller_Mouse X") * mouseSensitivity;
                if (!invertYCam)
                    _LocalRotation.y += Input.GetAxis("Controller_Mouse Y") * mouseSensitivity;
                else
                    _LocalRotation.y -= Input.GetAxis("Controller_Mouse Y") * mouseSensitivity;

            }

            Quaternion qt = Quaternion.Euler(_LocalRotation.y, _LocalRotation.x, 0);
            transform.localPosition = Vector3.Lerp(transform.localPosition, newCamPoint.localPosition, Time.deltaTime * mouseDamping);
            transform.localRotation = Quaternion.Lerp(transform.localRotation, newCamPoint.localRotation, Time.deltaTime * mouseDamping);

            target.rotation = Quaternion.Lerp(target.rotation, qt, Time.deltaTime * mouseDamping);
<<<<<<< HEAD
        }    
=======
>>>>>>> e7c686b5f6699887a47c21e7e3d30928a216dd1c

        }
    }


    private void OnEnable()
    {
        EventManager.SwitchMovement += Camera_HandleMovementSwitch;
    }

    private void OnDisable()
    {
        EventManager.SwitchMovement -= Camera_HandleMovementSwitch;
    }

    public Slider sensSlider;
    public void UpdateSens()
    {
         mouseSensitivity = sensSlider.value;
    }


    public void Camera_HandleMovementSwitch()
    {
        // Hard code all the changes that will happen to the camera when the player enters cave or leaves...

        //test:
        if (lockCamera)
        {
            GetComponent<Cinemachine.CinemachineBrain>().enabled = false;
            vcam.SetActive(false);

            camDisabled = false;
<<<<<<< HEAD


            transform.parent = target;

          //  this.transform = newCamPoint;
                     
            _LocalRotation.x = transform.parent.eulerAngles.y;
            _LocalRotation.y = transform.parent.eulerAngles.x;
=======
            // transform.parent = target;
            //  this.transform = newCamPoint;
            _LocalRotation.x = transform.eulerAngles.y;
            _LocalRotation.y = transform.eulerAngles.x;
>>>>>>> e7c686b5f6699887a47c21e7e3d30928a216dd1c
            _LocalRotation.z = 0;
            lockCamera = false;
            Debug.Log("Setting camera movement to freefrom movement");
        }
        else
        {
            //transform.parent = null;
            lockCamera = true;
            Debug.Log("Setting camera movement to contrained movement");
        }
    }


    public float DistanceToTarget()
    {
        return Vector3.Distance(transform.position, target.position);
    }

}
