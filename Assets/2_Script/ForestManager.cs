﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForestManager : MonoBehaviour
{
    public static ForestManager instance;

    public Animator credits;

   
    // Use this for initialization
    void Awake()
    {
        if (instance != null)
        {
            DestroyImmediate(this);
        }
        else
        {
            instance = this;
        }
    }

    public int lanternsActive;
    int lanternsCount;

    private void Start()
    {
      
        lanternsCount = FindObjectsOfType<LanternSwitch>().Length;
      
        
    }


    /// <summary>
    /// 6 lanterns.
    /// possible will be more than 6 comm orbs.
    /// need to activate chuncks of comm orbs.
    /// </summary>
    public void LanternActivated(GameObject[] orbsToAct)
    {
        for (int i = 0; i < orbsToAct.Length; i++)
        {
            orbsToAct[i].gameObject.SetActive(true);
        }
  
        lanternsActive++;

        if (lanternsActive >= lanternsCount)
        {
            credits.Play("CreditsAnimation", 0);
            Debug.Log("Fuck Off");
        }
    }
}