﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompanionController : MonoBehaviour {

    public float speedSmoothing;

    public Transform target;
    
    private void FixedUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, target.position, speedSmoothing * Time.deltaTime);    
    }
}
