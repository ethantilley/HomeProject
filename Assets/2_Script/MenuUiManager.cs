﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuUiManager : MonoBehaviour {

    //public int levelToLoad;

    public void OnClick_Play(int levelToLoad)
    {
        SceneManager.LoadScene(levelToLoad);
    }

    public void OnClick_Quit()
    {
        Application.Quit();
    }
}
