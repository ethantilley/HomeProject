﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour {

    BoxCollider bC;
    public GameObject panelEntry;
    public GameObject panelExit;

    public GameObject uiPrompt;


    // Use this for initialization
    void Start () {
        bC = GetComponent<BoxCollider>();
        bC.isTrigger = true;
	}
	
	// Update is called once per frame
	void Update () {
        //Checks if any input
        if (Input.anyKey == true)
        {
            uiPrompt.SetActive(false);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            uiPrompt.SetActive(true);

            bC.isTrigger = false;
            panelEntry.SetActive(false);
            panelExit.SetActive(true);
        }
    }
}
