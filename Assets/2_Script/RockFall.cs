﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockFall : MonoBehaviour {

    public GameObject Rocks;

    private void OnTriggerEnter(Collider other)
    {
        Rocks.SetActive(true);
    }

}
