﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Dev helper to reload current level on keypress
/// </summary>
public class ReloadLevel : MonoBehaviour
{
    public KeyCode keyCode = KeyCode.Backspace;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(keyCode))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        }
    }
}
