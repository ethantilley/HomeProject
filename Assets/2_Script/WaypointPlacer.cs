﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointPlacer : MonoBehaviour {

    public float pointSetterTick = 1;
    public WayPointData dataToSaveTo;
    public bool recording = false;
	// Use this for initialization
	void Start () {
        
        InvokeRepeating("SetWaypoint", 1, pointSetterTick);
	}

	void SetWaypoint()
    {
        if(recording)
            dataToSaveTo.wayPointPositions.Add(transform.position);
    }
    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (recording == false)
            {
                dataToSaveTo.wayPointPositions.Clear();
            }
            recording = !recording;
        }

        if (dataToSaveTo.wayPointPositions.Count > 0 && Input.GetKeyDown(KeyCode.L))
        {
            //set the waypoints


        }

	}
}
