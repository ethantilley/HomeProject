﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WayPointList")]
public class WayPointData : ScriptableObject {

    public List<Vector3> wayPointPositions = new List<Vector3>();

}
